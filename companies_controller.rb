class CompaniesController < ApplicationController
  def add_image
    respond_to do |format|
      format.html
      if params[:crop]
        format.js { render "#{layout_prefix}add_image_with_crop" }
      else
        format.js { render "#{layout_prefix}add_image" }
      end
    end
  end

  def update_image
    @field = params[:field_name]

    if @company.update_attributes(image_params)
      respond_to do |format|
        format.html { redirect_to edit_company_path(@company, notice: 'Файл был успешно загружен') }
        format.js { render "#{layout_prefix}update_image" }
      end
    else
      flash[:alert] = 'Что-то пошло не так'
      respond_to do |format|
        format.html { redirect_to edit_company_path(@company) }
        format.js { render "shared/errors" }
      end
    end
  end
end
