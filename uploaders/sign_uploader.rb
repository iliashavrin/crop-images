class SignUploader < ImageUploader
  MAX_HEIGHT = 70
  MAX_WIDTH = 120

  process :transparent_conversion => [MAX_WIDTH, MAX_HEIGHT]

  def transparent_conversion(width, height)
    manipulate! do |img|
      img.format('png') do |c|
        c.resize      "#{width}x#{height}"
        c.transparent 'white'
        c.fuzz        '20%'
      end
      img
    end
  end

  def filename
    super.chomp(File.extname(super)) + '.png' if super
  end

end
