function handleCompanyCropInput(){
	var img = $('.company-crop-cnt img')[0],
			previewImg = $('.company-crop-preview-cnt img')[0],
			widthCroppedImg = $(previewImg).css('width').replace('px',''),
			heightCroppedImg = $(previewImg).css('height').replace('px',''),
			sendCroppedImgBtn = $('#send-cropped-img-btn')[0],
			fileName = '';

	var cropper = new Cropper(img, {
		ready: function(){
			var canvas = this.cropper.getCroppedCanvas({width: widthCroppedImg, height: heightCroppedImg});
			previewImg.src = canvas.toDataURL();

			this.cropper.getCroppedCanvas().toBlob(function(blob){
				sendCroppedImgBtn.blob = blob;
				sendCroppedImgBtn.fileName = fileName;
			})			
		},
		cropend: function(){
			var canvas = this.cropper.getCroppedCanvas({width: widthCroppedImg, height: heightCroppedImg});
			previewImg.src = canvas.toDataURL();

			this.cropper.getCroppedCanvas().toBlob(function(blob){
				sendCroppedImgBtn.blob = blob;
				sendCroppedImgBtn.fileName = fileName;
			})
		}
	});

	$('#initial-crop-img').change(function(e){
		fileName = this.files[0].name;
		setInitialCropImg(this, cropper);
		$('#send-cropped-img-btn').prop('disabled', false);
	});
}


function setInitialCropImg(input, crop){
	if(input.files && input.files[0]){
		var reader = new FileReader();
		reader.onload = function(e){
			crop.replace(e.target.result)
		}
		reader.readAsDataURL(input.files[0]);
	}
}


function handleSendCroppedImageBtn(defaultFileName){
	var field = $('#field_name'),
			fieldName = field.val(),
			formData = new FormData();

	$('#send-cropped-img-btn').click(function(e){
		var fileName = this.fileName || defaultFileName;

		formData.append('field_name', fieldName);
		formData.append('need_add_png', true);
		formData.append('company[' + fieldName + ']', this.blob, fileName);

		$.ajax('/companies/' + field.data('companyId') + '/update_image.js', {
			method: 'POST',
			data: formData,
			processData: false,
			contentType: false,
			success: function(){},
			error: function(){console.log('In method handleSendCroppedImageBtn error');}
		});
	})
}
